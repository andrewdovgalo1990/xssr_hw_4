﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using FMOD.Studio;


public class HealthCheck : MonoBehaviour
{
    public vThirdPersonController tpController;
    [FMODUnity.EventRef] public string HealthSnapshot;
    [FMODUnity.EventRef] public string HealthEvent;
    //FMOD.Studio.EventInstance HealthInstance;
    FMOD.Studio.EventInstance SnapInstance;
    public FMOD.Studio.PLAYBACK_STATE state;


    void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        SnapInstance = FMODUnity.RuntimeManager.CreateInstance(HealthSnapshot);
        FMODUnity.RuntimeManager.PlayOneShotAttached(HealthEvent, gameObject);
        //HealthInstance = FMODUnity.RuntimeManager.CreateInstance(HealthEvent);
        //HealthInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        //HealthInstance.start();
    }

    void Update()

    {
        SnapInstance.getPlaybackState(out state);
        //HealthInstance.getPlaybackState(out state);
       //HealthInstance.setParameterByName("HealthBar", tpController.currentHealth);

        if (tpController.currentHealth <= 30)
        {
            if (state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                SnapInstance.start();
                //HealthInstance.start();

            }
        }
        else
        {
            if (state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                SnapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                //HealthInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                
            }

        }
        if (tpController.isDead == true)
        {
            SnapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            //HealthInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }

    }
}
