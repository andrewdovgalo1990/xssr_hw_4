﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;
using Invector.vCharacterController;

public class InteractiveMusicSystem : MonoBehaviour
{
    public vThirdPersonController tpController;
    [FMODUnity.EventRef] public string musicEvent;
    private FMOD.Studio.EventInstance musicInstatce;
    public vControlAIMelee enemy;
    public vControlAIMelee enemy1;
    public vControlAIMelee enemy2;
    public bool inCombat = false;

    // Start is called before the first frame update
    void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        musicInstatce = FMODUnity.RuntimeManager.CreateInstance(musicEvent);
        musicInstatce.start();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inCombat && enemy1.isInCombat)
        {
            musicInstatce.setParameterByName("GameState", 1f);
            inCombat = true;

        }
        else if (inCombat && enemy1.isInCombat == false)
        {
            musicInstatce.setParameterByName("GameState", 0f);
            inCombat = false;


        }
        if (!inCombat && enemy2.isInCombat)
        {
            musicInstatce.setParameterByName("GameState", 1f);
            inCombat = true;

        }
        else if (inCombat && enemy2.isInCombat == false)
        {
            musicInstatce.setParameterByName("GameState", 0f);
            inCombat = false;


        }
    }
        /*if (tpController.isDead == true)
        {
            musicInstatce.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }*/
    

}
