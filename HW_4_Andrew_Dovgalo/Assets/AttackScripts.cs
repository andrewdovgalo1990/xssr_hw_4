﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScripts : MonoBehaviour
{
    [FMODUnity.EventRef] public string HitEvent;
    [FMODUnity.EventRef] public string DamageEvent;
    [FMODUnity.EventRef] public string DeathEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Attack_Hit()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(HitEvent, gameObject);
    }

    void Damage()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(DamageEvent, gameObject);
    }

    void Death()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(DeathEvent, gameObject);
    }
}
