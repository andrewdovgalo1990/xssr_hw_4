﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHighScripts : MonoBehaviour
{
    [FMODUnity.EventRef] public string MountainWindEvent;
    FMOD.Studio.EventInstance MountainWindInstance;
    public GameObject cameraObject;

    // Start is called before the first frame update
    void Start()
    {
        MountainWindInstance = FMODUnity.RuntimeManager.CreateInstance(MountainWindEvent);
        MountainWindInstance.start();

    }

    // Update is called once per frame
    void Update()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("CameraHeight", cameraObject.transform.position.y);
        //MountainWindInstance.setParameterByName("CameraHeight", cameraObject.transform.position.y);
    }
}
