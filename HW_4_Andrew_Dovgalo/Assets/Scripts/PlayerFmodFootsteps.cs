﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class PlayerFmodFootsteps : MonoBehaviour
{
    [FMODUnity.EventRef] public string walkingEvent;

    [FMODUnity.EventRef] public string runningEvent;

    vThirdPersonInput tpInput;
    FMOD.Studio.EventInstance walkingInstance;
    vThirdPersonController tpController;

    [FMODUnity.EventRef] public string breathEvent;
    [FMODUnity.EventRef] public string jumpEvent;
    [FMODUnity.EventRef] public string landEvent;


    FMOD.Studio.EventInstance breathInstance;
    FMOD.Studio.EventInstance landInstance;
    FMOD.Studio.EventInstance jumpInstance;

    public LayerMask lm;
    float surface;




    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
        breathInstance = FMODUnity.RuntimeManager.CreateInstance(breathEvent);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(breathInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        breathInstance.start();


    }


    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {


            SurfaceCheck();
            if (tpController.isJumping == false)
            {


                if (tpController.isSprinting)
                {
                    walkingInstance = FMODUnity.RuntimeManager.CreateInstance(runningEvent);
                    FMODUnity.RuntimeManager.AttachInstanceToGameObject(walkingInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
                    breathInstance.setParameterByName("locomotion_type", 2f);
                    walkingInstance.setParameterByName("SurfaceType", surface);
                    walkingInstance.start();
                    walkingInstance.release();
                }

                else
                {
                    walkingInstance = FMODUnity.RuntimeManager.CreateInstance(walkingEvent);
                    FMODUnity.RuntimeManager.AttachInstanceToGameObject(walkingInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
                    breathInstance.setParameterByName("locomotion_type", 1f);
                    walkingInstance.setParameterByName("SurfaceType", surface);
                    walkingInstance.start();
                    walkingInstance.release();
                }
            }

        }



    }
    void jump()
    {

        if (tpController.isJumping)
        {
            jumpInstance = FMODUnity.RuntimeManager.CreateInstance(jumpEvent);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(jumpInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
            jumpInstance.setParameterByName("locomotion_type", 6f);
            jumpInstance.setParameterByName("SurfaceType", surface);
            jumpInstance.start();
            jumpInstance.release();

            //breathInstance.setParameterByName("locomotion_type", 3f);
            Debug.Log("Jump123");
        }
    }


    void jump_move()
    {

        if (tpController.isJumping)
        {
            jumpInstance = FMODUnity.RuntimeManager.CreateInstance(jumpEvent);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(jumpInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
            jumpInstance.setParameterByName("locomotion_type", 6f);
            jumpInstance.setParameterByName("SurfaceType", surface);
            jumpInstance.start();
            jumpInstance.release();

            // breathInstance.setParameterByName("locomotion_type", 4f);
            Debug.Log("Jump Move");
        }
    }

    void falling()
    {
        SurfaceCheck();
        landInstance = FMODUnity.RuntimeManager.CreateInstance(landEvent);
        landInstance.setParameterByName("locomotion_type", 5f);
        landInstance.setParameterByName("SurfaceType", surface);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(landInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        landInstance.start();
        landInstance.release();
        // breathInstance.setParameterByName("locomotion_type", 5f);
        Debug.Log("Falling");


    }

    /*void land_low()
    {
        SurfaceCheck();
        landInstance = FMODUnity.RuntimeManager.CreateInstance(landEvent);
        landInstance.setParameterByName("locomotion_type", 5f);
        landInstance.setParameterByName("SurfaceType", surface);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(landInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        landInstance.start();
        landInstance.release();
        breathInstance.setParameterByName("locomotion_type", 5f);
        Debug.Log("Landing Low");


    }

    void land_high()
    {
        SurfaceCheck();
        landInstance = FMODUnity.RuntimeManager.CreateInstance(landEvent);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(landInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        landInstance.setParameterByName("locomotion_type", 6f);
        landInstance.setParameterByName("SurfaceType", surface);
        landInstance.start();
        landInstance.release();
        breathInstance.setParameterByName("locomotion_type", 6f);
        Debug.Log("Landing High");
    }
    */


    void SurfaceCheck()
    {

        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm))
        {
            Debug.Log(hit.collider.tag);

            switch (hit.collider.tag)
            {
                case "Snow":
                    surface = 0f;
                    break;
                case "Wood_Snow":
                    surface = 1f;
                    break;
                case "Sand":
                    surface = 2f;
                    break;
                case "Wood":
                    surface = 3f;
                    break;
                case "Water":
                    surface = 4f;
                    break;
                /*case "vNear_water":
                    surface = 5f;
                    break;
                case "vIce_stead":
                    surface = 6f;
                    break;
                case "vIce":
                    surface = 7f;
                    break;
                case "vWood_snow":
                    surface = 8f;
                    break;*/
                default:
                    surface = 0f;
                    break;
            }

        }

    }

}




