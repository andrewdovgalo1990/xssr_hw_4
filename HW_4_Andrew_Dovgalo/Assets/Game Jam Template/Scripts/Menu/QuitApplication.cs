﻿using UnityEngine;
using System.Collections;

public class QuitApplication : MonoBehaviour 
{
	[FMODUnity.EventRef] public string quitEvent;
	FMOD.Studio.EventInstance quitInstance;

    void Awake()
    {
		quitInstance = FMODUnity.RuntimeManager.CreateInstance(quitEvent);
	}

    public void Quit()
	{
		//If we are running in a standalone build of the game
#if UNITY_STANDALONE
		//Quit the application
		quitInstance.start();
		Application.Quit();
	#endif

		//If we are running in the editor
	#if UNITY_EDITOR
		//Stop playing the scene
		UnityEditor.EditorApplication.isPlaying = false;
	#endif
	}
}
